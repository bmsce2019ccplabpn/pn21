#include <stdio.h>
int input();
int compute(int);
void output(int);
int main()
{
    int a,res;
    a=input();
    res=compute(a);
    output(res);
    return 0;
}
int input()
{
    int x;
    printf("enter the number");
    scanf("%d",&x);
    return x;
}
int compute(int a)
{
    int sum=0;
    while(a!=0)
    {
        sum=sum+(a%10);
        a=a/10;
    } 
    return sum;
}
void output(int res)
{
    printf("the sum of the number is %d",res);
}