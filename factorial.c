#include <stdio.h>
#include <math.h>
int fac(int);
int main()
{
    int n,i;
    float sum=0.0;
    printf("enter the no. of term");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        sum+=(float)i/fac(i);
    }
    printf("sum of the term=%f",sum);
    return 0;
}
int fac(int n)
{
    int f=1,i;
    for(i=1;i<=n;i++)
    {
        f=f*i;
    }
    return f;
}