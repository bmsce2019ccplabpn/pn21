#include <stdio.h>

int convert_to_minutes(int h,int m)
{
    int total_minutes;
    total_minutes=(h*60)+m;
    return total_minutes;
}
int main()
{
    int Hours,Minutes,total_minutes;
    printf("\nEnter the time in Hour and Minutes: \n");
    printf("\nHour: \t");
    scanf("%d",&Hours);
    printf("\nMinutes: \t");
    scanf("%d",&Minutes);
    total_minutes=convert_to_minutes(Hours,Minutes);
    printf("\nTotal_time_to_minutes: \t%d\n",total_minutes);
    return 0;
}